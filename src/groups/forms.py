from django.core.exceptions import ValidationError
from django.forms import ModelForm
from groups.models import Group
from datetime import datetime


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ["course_name", "course_id", "lesson_count", "start_date_course", "end_date_course"]

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    @staticmethod
    def normalize_date(date_str: str) -> datetime.date:
        try:
            date = datetime.strptime(date_str, "%Y-%m-%d").date()
            return date
        except ValueError:
            raise ValidationError("Invalid date format. Please use YYYY-MM-DD.")

    def clean_course_name(self):
        course_name = self.cleaned_data["course_name"]
        return self.normalize_text(course_name)

    def clean_start_date_course(self):
        start_date = self.cleaned_data["start_date_course"]
        start_date_str = start_date.strftime("%Y-%m-%d")
        return self.normalize_date(start_date_str)

    def clean_end_date_course(self):
        end_date = self.cleaned_data["end_date_course"]
        end_date_str = end_date.strftime("%Y-%m-%d")
        return self.normalize_date(end_date_str)

    def clean(self):
        cleaned_data = super().clean()

        start_date_course = cleaned_data["start_date_course"]
        end_date_course = cleaned_data["end_date_course"]

        if start_date_course == end_date_course:
            raise ValidationError("Courses cannot last one day!")

        return cleaned_data
