from django.urls import path
from groups.views import get_groups, GroupUpdateView, GroupCreate, GroupDeleteView

app_name = "groups"

urlpatterns = [
    path("", get_groups, name="get_groups"),
    path("create/", GroupCreate.as_view(), name="create_group"),
    path("update/<int:pk>/", GroupUpdateView.as_view(), name="update_group"),
    path("delete/<int:pk>/", GroupDeleteView.as_view(), name="delete_group"),
]
