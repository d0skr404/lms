from django.db import models
from faker import Faker


class Group(models.Model):
    course_name = models.CharField(max_length=120, null=True, blank=True)
    course_id = models.IntegerField(null=True, blank=True)
    max_count_of_students = models.PositiveSmallIntegerField(default=20, null=True, blank=True)
    lesson_count = models.PositiveSmallIntegerField(null=True, blank=True)
    homework_count = models.PositiveSmallIntegerField(null=True, blank=True)
    start_date_course = models.DateField(null=True, blank=True)
    end_date_course = models.DateField(null=True, blank=True)
    group_gpa = models.DecimalField(max_digits=100, decimal_places=0, null=True, blank=True)

    @classmethod
    def generate_instance(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                course_name=faker.sentence(nb_words=3),
                course_id=faker.random_int(min=1, max=1000),
                max_count_of_students=faker.random_int(min=1, max=18),
                lesson_count=faker.random_int(min=1, max=32),
                homework_count=faker.random_int(min=1, max=32),
                start_date_course=faker.date_between(start_date="-30d", end_date="+30d"),
                end_date_course=faker.date_between(start_date="+30d", end_date="+90d"),
                group_gpa=faker.pydecimal(positive=True, min_value=1, max_value=100),
            )

    def __str__(self):
        return (
            f"{self.course_name} {self.course_id} {self.lesson_count} {self.start_date_course} {self.end_date_course}"
        )
