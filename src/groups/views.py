from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView, CreateView
from webargs import fields
from webargs.djangoparser import use_args

from groups.forms import GroupForm

from groups.models import Group


@use_args(
    {
        "course_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_groups(request, params):
    groups = Group.objects.all()

    search_fields = ["course_name", "course_id", "lesson_count", "start_date_course", "end_date_course"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})

    return render(request, template_name="group_list.html", context={"groups": groups})


class GroupCreate(LoginRequiredMixin,CreateView):
    template_name = "create.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")


class GroupUpdateView(LoginRequiredMixin,UpdateView):
    template_name = "update.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")
    queryset = Group.objects.all()
    pk_url_kwarg = "pk"


class GroupDeleteView(LoginRequiredMixin,DeleteView):
    template_name = "delete.html"
    success_url = reverse_lazy("groups:get_groups")
    queryset = Group.objects.all()
    pk_url_kwarg = "pk"
