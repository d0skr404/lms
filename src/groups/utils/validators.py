from django.core.exceptions import ValidationError


def course_name_validator(course_name: str) -> None:
    if " " in course_name.lower():
        raise ValidationError("the title is required!")
