import datetime

from django.db import models
from faker import Faker

from core.models import Person


class Teacher(Person):
    department = models.CharField(max_length=300, null=True, blank=True)
    group = models.ManyToManyField(to="groups.Group")
    student_count = models.PositiveSmallIntegerField(null=True, blank=True)
    group_count = models.PositiveSmallIntegerField(null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                student_count=faker.random_int(min=25, max=100),
                group_count=faker.random_int(min=2, max=5),
            )

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.pk})"
