from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from webargs import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_teachers(request, params):
    teachers = Teacher.objects.all()
    search_fields = ["first_name", "last_name", "email", "group"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})

    return render(request, template_name="teacher_list.html", context={"teachers": teachers})


class TeacherCreate(LoginRequiredMixin, CreateView):
    template_name = "create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "update.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")
    queryset = Teacher.objects.all()
    pk_url_kwarg = "pk"


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "delete.html"
    success_url = reverse_lazy("teachers:get_teachers")
    queryset = Teacher.objects.all()
    pk_url_kwarg = "pk"
