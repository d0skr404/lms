# Generated by Django 4.2.2 on 2023-07-20 21:46

import django.core.validators
from django.db import migrations, models
import core.utils.validators


class Migration(migrations.Migration):
    dependencies = [
        ("groups", "0003_group_delete_groups"),
        ("teachers", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="teacher",
            name="course_end_date",
        ),
        migrations.RemoveField(
            model_name="teacher",
            name="course_start_date",
        ),
        migrations.AddField(
            model_name="teacher",
            name="department",
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name="teacher",
            name="grade",
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="teacher",
            name="group",
            field=models.ManyToManyField(to="groups.group"),
        ),
        migrations.AlterField(
            model_name="teacher",
            name="first_name",
            field=models.CharField(
                blank=True,
                max_length=120,
                null=True,
                validators=[
                    django.core.validators.MinLengthValidator(2),
                    core.utils.validators.first_name_validator,
                ],
            ),
        ),
    ]
