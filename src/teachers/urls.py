from django.urls import path
from teachers.views import TeacherCreate, TeacherUpdateView, get_teachers, TeacherDeleteView

app_name = "teachers"

urlpatterns = [
    path("", get_teachers, name="get_teachers"),
    path("create/", TeacherCreate.as_view(), name="create_teacher"),
    path("update/<int:pk>/", TeacherUpdateView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", TeacherDeleteView.as_view(), name="delete_teacher"),
]
