import datetime

from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView
from core.forms import UserRegistrationForm


class UserLoginView(LoginView):
    ...


class UserLogoutView(LogoutView):
    ...


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]


class UserRegistrationView(CreateView):
    template_name = "registration/create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        _form = super().form_valid(form)
        login(self.request, self.object)
        return _form


def send_test_email(request):
    send_mail(
        subject="Hello from LMS",
        message=f"Hello from Denis Oskorib, {datetime.datetime.now()}",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[
            settings.EMAIL_HOST_USER,
            # "mikolaz2727@gmail.com",
            "denisoskorib1995@gmail.com",
        ],
    )
    return HttpResponse("Done")
