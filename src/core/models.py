from django.core.validators import MinLengthValidator
from django.db import models

from core.utils.validators import first_name_validator


class Person(models.Model):
    first_name = models.CharField(
        max_length=120, null=True, blank=True, validators=[MinLengthValidator(2), first_name_validator]
    )
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    grade = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        abstract = True
