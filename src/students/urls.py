from django.urls import path
from students.views import get_students, StudentCreate, StudentUpdateView, StudentDeleteView

app_name = "students"

urlpatterns = [
    path("", get_students, name="get_students"),
    path("create/", StudentCreate.as_view(), name="create_student"),
    path("update/<uuid:uuid>/", StudentUpdateView.as_view(), name="update_student"),
    path("delete/<uuid:uuid>/", StudentDeleteView.as_view(), name="delete_student"),
]
