import datetime
from uuid import uuid4

from django.db import models
from faker import Faker
from core.models import Person
from groups.models import Group


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    group = models.ForeignKey(to="groups.Group", on_delete=models.CASCADE)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                group=Group.objects.get(id=1),
            )

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.pk})"
