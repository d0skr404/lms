from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView, DeleteView
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request, params):
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return render(request, template_name="student_list.html", context={"students": students})


class StudentCreate(LoginRequiredMixin, CreateView):
    template_name = "create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "update.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")
    queryset = Student.objects.all()
    pk_url_kwarg = "uuid"


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "delete.html"
    success_url = reverse_lazy("students:get_students")
    queryset = Student.objects.all()
    pk_url_kwarg = "uuid"
