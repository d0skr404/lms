# Generated by Django 4.2.2 on 2023-07-20 21:46

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import core.utils.validators
import uuid


class Migration(migrations.Migration):
    dependencies = [
        ("groups", "0003_group_delete_groups"),
        ("students", "0005_student_birth_date_student_email_student_grade"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="student",
            name="id",
        ),
        migrations.AddField(
            model_name="student",
            name="group",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to="groups.group",
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="student",
            name="uuid",
            field=models.UUIDField(
                db_index=True,
                default=uuid.uuid4,
                editable=False,
                primary_key=True,
                serialize=False,
                unique=True,
            ),
        ),
        migrations.AlterField(
            model_name="student",
            name="first_name",
            field=models.CharField(
                blank=True,
                max_length=120,
                null=True,
                validators=[
                    django.core.validators.MinLengthValidator(2),
                    core.utils.validators.first_name_validator,
                ],
            ),
        ),
        migrations.AlterField(
            model_name="student",
            name="last_name",
            field=models.CharField(blank=True, max_length=120, null=True),
        ),
    ]
